plugins {
    id("java")
    application
}

group = "week5.task1.a"
version = "1.0-SNAPSHOT"
val mainClazz = "week5.task1.a.JavaPerms"

repositories {
    mavenCentral()
}

application {
    mainClass.set(mainClazz)
}

dependencies {
    implementation("org.jfree:jfreechart:1.5.3")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = mainClazz
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    val sourcesMain = sourceSets.main.get()
    from(sourcesMain.output)
}
