package week2;

import java.util.Scanner;

public class Task1_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Месяц(1-12): ");
        int month = in.nextInt();

        System.out.print("Год: ");
        int year = in.nextInt();

        if (month == 4 | month == 6 | month == 9 | month == 11) {
            System.out.print("Количество дней в месяце = 30");
        }
        else if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) {
            System.out.print("Количество дней в месяце = 31");
        }
        else if (month == 2 & (year % 4 == 0) & (year % 100 == 0) & (year % 400 == 0) ) {
            System.out.print("Количество дней в месяце = 29");
        }
        else {
            System.out.print("Количество дней в месяце = 28");
        }
    }
}
