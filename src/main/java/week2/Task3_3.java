package week2;

import java.util.Arrays;
import java.util.Scanner;

public class Task3_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Объявляем Scanner
        System.out.println("Enter array length: ");
        int size = input.nextInt(); // Читаем с клавиатуры размер массива и записываем в size
        int[] array = new int[size]; // Создаём массив int размером в size
        System.out.println("Insert array elements:");
        /*Пройдёмся по всему массиву, заполняя его*/
        for (int i = 0; i < size; i++) {
            array[i] = input.nextInt(); // Заполняем массив элементами, введёнными с клавиатуры
        }
        Arrays.sort(array);
        double median;  
        if (array.length % 2 == 0)   // Поиск медианы
            median = ((double)array[array.length/2] + (double)array[array.length/2 - 1])/2;
        else
            median = array[array.length/2];
        System.out.print ("Inserted array elements:");
        for (int i = 0; i < size; i++) {
            System.out.print (" " + array[i]); // Выводим на экран, полученный массив
        }
        System.out.printf("\n Median = %.1f", median);
    }
}
