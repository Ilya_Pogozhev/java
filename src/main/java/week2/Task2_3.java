package week2;

import java.util.Arrays;
import java.util.Scanner;

public class Task2_3 {
    public static void sieve(int num){
        var sieve = new boolean[num];
        Arrays.fill(sieve, true);
        sieve[0] = false;
        sieve[1] = false;

        for (int i = 2; i * i < num; i++) {
            if (sieve[i]) {
                for (int j = i * i; j < num; j+= i){
                    sieve[j] = false;
                }
            }
        }
        System.out.print("Простые числа: ");
        for (int i = 2; i < num; i++) {
            if (sieve[i]) {
                System.out.print(i + " ");
            }
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите максимум поиска простых чисел: ");
        int num = in.nextInt();
        sieve(num);
    }
}
