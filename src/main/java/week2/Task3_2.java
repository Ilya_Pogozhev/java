package week2;

import java.util.Scanner;
public class Task3_2 {
    public static void mergeSort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        System.arraycopy(a, 0, l, 0, mid);
        if (n - mid >= 0) System.arraycopy(a, mid, r, 0, n - mid);
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }

    public static void merge(
            int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            } else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите числа для сортировки: ");
            String numbers = scanner.nextLine();
            String[] mas = numbers.split(" ");
            int[] masInt = new int[mas.length];
            for (int i = 0; i < mas.length; i++) {
                masInt[i] = Integer.parseInt(mas[i]);
            }
            mergeSort(masInt, masInt.length);
            System.out.println("Отсортированный массив: ");
            for (int j : masInt) System.out.print(j + " ");
        }
    }
}

