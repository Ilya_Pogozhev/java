package week2;

import java.util.Scanner;
public class Task3_1 {
    public static void count(String nums) {
        String[] StringNumbers = nums.split(" ");
        double[] numbers = new double[StringNumbers.length];
        for (int i = 0; i < StringNumbers.length; i++) {
            numbers[i] = Double.parseDouble(StringNumbers[i]);
        }
        double max = numbers[0];
        for (double number : numbers) {
            if (number > max) {
                max = number;
            }
        }
        int count = 0;
        for (double number : numbers) {
            if (max == number) {
                count++;
            }
        }
        System.out.println(count);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите массив(через пробел): ");
        String nums = in.nextLine();
        count(nums);
        }
}
