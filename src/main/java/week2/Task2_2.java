package week2;

import java.util.Scanner;
public class Task2_2 {
    static void geron_sqrt(long a) {
        double b = a;
        int i = 0;
        // Итерационная формула Герона
        while ((b*b>a)&&(i<200)) {
            b = (b+a/b)/2;
            i++;
        }
        System.out.println(b);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        int num = in.nextInt();
        geron_sqrt(num);
    }
}
