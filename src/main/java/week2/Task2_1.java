package week2;

import java.util.Scanner;

public class Task2_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите высоту пирамиды: ");
        int height = in.nextInt();

        System.out.println(piramide(height));
    }

    public static boolean piramide(int height) {
        for (int i = 0; i < height; i++) {
            for (int x = 0; x <= height - i; x++) {
                System.out.print(" ");
            }
            for (int j = 2; j <= i + 1; j++) {
                System.out.print("#");
            }
            System.out.print(" ");
            for (int j = 2; j <= i + 1; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        return false;
    }
}




