package week2;

import java.util.Scanner;
// Хотите ли вы играть в бадминтон?
public class Task1_3 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Температура(жарко/тепло/холодно): ");
            String temperature = in.nextLine();
            switch(temperature){
                case "жарко","тепло" -> {
                    System.out.print("Осадки(ясно/облачно/дождь/снег/град): ");
                    String precipitation = in.nextLine();
                    switch(precipitation){
                        case "ясно", "облачно" -> {
                            System.out.print("Ветер(есть/нет): ");
                            String wind = in.nextLine();
                            if ("нет".equals(wind)) {
                                System.out.print("Влажность(высокая/средняя/низкая): ");
                                String humidity = in.nextLine();
                                if ("средняя".equals(humidity)) {
                                    System.out.println("Да");
                                } else {
                                    System.out.println("Нет");
                                }
                            } else {
                                System.out.println("Нет");
                            }
                        }
                        default -> System.out.println("Нет");
                    }
                }
                default -> System.out.println("Нет");
            }
        }
    }

}


