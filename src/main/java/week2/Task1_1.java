package week2;

import java.util.Scanner;

public class Task1_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input age: ");
        int age = in.nextInt();

        if (age % 10 == 1 && age != 11 && age != 111) {
            System.out.printf("%d год", age);
        }
        else if (age%10>1 && age%10<5 && age!=12 && age!=13 && age!=14) {
            System.out.printf(" %d года", age);
        }
        else {
            System.out.printf(" %d лет", age);
        }
        in.close();
    }
}

