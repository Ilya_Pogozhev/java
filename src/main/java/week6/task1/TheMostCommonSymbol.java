package week6.task1;

import java.util.*;

public class TheMostCommonSymbol {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();

        String[] split = s.replaceAll("'", "").split("");
        Map<String, Long> map = new TreeMap<>();

        maxValueCount(split, map);
        System.out.printf("['%s', %s]", maxValue(map), map.get(maxValue(map)));
    }

    private static String maxValue(Map<String, Long> map) {
        return map.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
    }

    private static void maxValueCount(String[] split, Map<String, Long> map) {
        for (String str: split) {
            long count = Arrays.stream(split).filter(p -> p.equals(str)).count();
            map.put(str, count);
        }
    }
}
