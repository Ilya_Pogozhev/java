package week5.task3.a;

import java.util.*;

public class RemoveDuplicates {
    public static LinkedHashSet<String> cleaningArr(String[] arr) {
        return new LinkedHashSet<>(Arrays.asList(arr));

    }

    public static void main(String[] args) {
        Scanner inputStr = new Scanner(System.in);
        Scanner inputInt = new Scanner(System.in);

        System.out.println("Введите количество элементов: ");
        int count = inputInt.nextInt();
        String[] arr = new String[count];

        System.out.println("Введите элементы массива: ");
        extracted(inputStr, count, arr);

        System.out.printf("Результат: %s", cleaningArr(arr).
                toString().
                replaceAll("^\\[|]$", ""));
    }

    private static void extracted(Scanner inputStr, int count, String[] arr) {
        for (int i = 0; i < count; i++) {
            arr[i] = inputStr.nextLine();
        }
    }
}

