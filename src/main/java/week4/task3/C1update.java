package week4.task3;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;
import java.lang.NumberFormatException;

public class C1update {
    public static void main(String[] args) throws CheckChoice  {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Введите показания датчиков с разделителем @: ");
            String readings = in.nextLine();
            System.out.print("Введите 1 - сортировать по id, 2 - по температуре ");
            int choice = in.nextInt();
            Integer[][] result = DataOfSensors(readings);
            switch (choice) {
                case 1 -> {
                    Arrays.sort(result, Comparator.comparingInt(o -> o[0]));
                    for (Integer[] integers : result) System.out.println(integers[0] + " " + integers[1]);
                }
                case 2 -> {
                    Arrays.sort(result, Comparator.comparingInt(o -> o[1]));
                    for (Integer[] integers : result) System.out.println(integers[0] + " " + integers[1]);
                }
                default -> throw new CheckChoice("Способ сортировки не выбран");
            }
        }
        catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public static Integer[][] DataOfSensors(String readings) {
        String[] text = readings.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++){
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2));
        }
        return output;
    }
}