package week4.task3;

import java.util.InputMismatchException;

public class CheckChoice extends InputMismatchException  {
    public CheckChoice(String message) {
        super(message);
    }
}
