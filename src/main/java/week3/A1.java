package week3;

import java.util.Scanner;

public class A1 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Enter string: ");
            String[] source = in.nextLine().split(" ");
            System.out.print("Enter substring: ");
            String sentence = in.nextLine();
            System.out.println(numberOfOccurrences(source,sentence));
        }
    }

    public static int numberOfOccurrences(String[] strings, String substring){
        int occurrences = 0;
        int i = -1;
        for (String str: strings){
            while ((i = str.indexOf(substring, i+1)) > -1) ++occurrences;
        }
        return occurrences;
    }
}
