package week3;

import java.util.Scanner;

public class C2 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Введите слово: ");
            String str = in.nextLine();
            str = str.replaceAll("(.)\\1{2,}", "$1");
            System.out.println(str);
        }
    }
}
