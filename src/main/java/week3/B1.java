package week3;

import java.util.Scanner;
// Рассчет энтропии по Шеннону
public class B1 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Enter text: ");
            String str = input.nextLine();
            System.out.printf("%.2f%n", entropy(str));
        }
    }

    public static double entropy(String str) {

        double[] counts = new double[Character.MAX_VALUE];
        int sum = 0;
        double result = 0.0;

        for (int i = 0; i < str.length(); i++) {
            char symbol = str.charAt(i);
            counts[symbol]++;
        }
        for (double count : counts) {
            if (count > 0)
                sum += count;
        }
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > 0)
                counts[i] /= sum;
        }
        for (double count : counts) {
            if (count > 0)
                result += count * log2(count);
        }
        return -1 * result;
    }

    public static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }
}
