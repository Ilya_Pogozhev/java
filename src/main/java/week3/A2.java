package week3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class A2 {
    public static boolean MailCheck(String email) {
        String emailRegex = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Введите адрес электронной почты: ");
            String email = in.nextLine();
            System.out.println("Адрес: " + email + (MailCheck(email) ? " Верный" : " Неверный"));
        }
    }
}





