package week3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class C1 {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("Введите показания датчиков с разделителем @: ");
            String readings = in.nextLine();
            System.out.print("Введите 1 - сортировать по id, 2 - по температуре ");
            int mode = in.nextInt();
            Integer[][] result = DataOfSensors(readings);
            switch (mode) {
                case 1 -> {
                    Arrays.sort(result, Comparator.comparingInt(o -> o[0]));
                    for (Integer[] integers : result) System.out.println(integers[0] + " " + integers[1]);
                }
                case 2 -> {
                    Arrays.sort(result, Comparator.comparingInt(o -> o[1]));
                    for (Integer[] integers : result) System.out.println(integers[0] + " " + integers[1]);
                }
            }
        }
    }

    public static Integer[][] DataOfSensors(String readings) {
        String[] text = readings.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++) {
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2));
        }
        return output;
    }
}

