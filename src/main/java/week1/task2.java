package week1;

import java.util.Scanner;
//Программа рассчета обьема икосаэдра
public class task2 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину ребра икосаэдра: ");
        double a = in.nextDouble();
        double result = 5.0/12 * (3 + Math.sqrt(5)) * Math.pow(a, 3);

        System.out.printf("Объем  икосаэдра: %.3f \n", result);
        in.close();
    }
}
