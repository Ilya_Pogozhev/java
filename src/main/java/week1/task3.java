package week1;

public class task3 {
    public static void main (String[] args) {

        System.out.println ("Минимальное значение байта =" + Byte.MIN_VALUE);
        System.out.println ("Максимальное значение байта = " + Byte.MAX_VALUE); // тип данных представляет собой 8-битное целое число в дополнении до двух со знаком. Он имеет минимальное значение -128 и максимальное значение 127 (включительно).
        System.out.println ("Мин. Короткое значение =" + Short.MIN_VALUE);
        System.out.println ("Макс. Короткое значение =" + Short.MAX_VALUE); // тип данных представляет собой 16-разрядное целое число в дополнении до двух со знаком. Он имеет минимальное значение -32 768 и максимальное значение 32 767 (включительно).
        System.out.println ("Min int value =" + Integer.MIN_VALUE);
        System.out.println ("Max int value =" + Integer.MAX_VALUE); // по умолчанию int тип данных представляет собой 32-разрядное целое число в дополнении до двух со знаком, минимальное значение которого равно -2^31 , а максимальное - 2^31 -1. 
        System.out.println ("Min long value =" + Long.MIN_VALUE);
        System.out.println ("Макс. Длинное значение =" + Long.MAX_VALUE); // тип данных представляет собой 64-битное целое число с дополнением до двух.
        System.out.println ("Минимальное значение с плавающей запятой =" + Float.MIN_VALUE); 
        System.out.println ("Max float value =" + Float.MAX_VALUE); // тип данных представляет собой 32-разрядное число с плавающей запятой одинарной точности IEEE 754.
        System.out.println ("Минимальное двойное значение =" + Double.MIN_VALUE);
        System.out.println ("Max double value =" + Double.MAX_VALUE); // тип данных представляет собой 64-разрядную двойную точность с плавающей запятой IEEE 754. 
    }
}
